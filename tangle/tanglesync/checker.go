package tanglesync

import (
	"../../db"
	"../../db/ns"
)

/*
Returns whether the current tangle is synchronized
*/
// TODO: this check is too slow on bigger databases. The counters should be moved to memory.
func IsSynchronized() bool {
	var isSync bool
	db.Singleton.View(func(dbTx db.Transaction) error {
		isSync = ns.Count(dbTx, ns.NamespacePendingConfirmed) < 10 &&
			ns.Count(dbTx, ns.NamespaceEventConfirmationPending) < 10 &&
			ns.Count(dbTx, ns.NamespaceEventMilestonePending) < 5
		return nil
	})
	return isSync
}
