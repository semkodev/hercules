package tangle

import (
	"bytes"
	"fmt"
	"sync"
	"time"

	"../convert"
	"../db"
	"../db/coding"
	"../db/ns"
	"../logs"
	"../snapshot"
	"../transaction"
	"github.com/pkg/errors"
)

const (
	UNKNOWN_CHECK_INTERVAL      = time.Duration(60) * time.Second
	confirmationsTickerInterval = time.Duration(2) * time.Second
)

var (
	pendingConfirmationsQueue       = make(chan *PendingConfirmation, maxQueueSize)
	pendingConfirmationsWaitGroup   = &sync.WaitGroup{}
	pendingConfirmationsQueueQuit   = make(chan struct{})
	pendingConfirmationsTicker      *time.Ticker
	removeOrphanedPendingTicker     *time.Ticker
	removeOrphanedPendingWaitGroup  = &sync.WaitGroup{}
	removeOrphanedPendingTickerQuit = make(chan struct{})
)

type PendingConfirmation struct {
	key       []byte
	timestamp int64
}

func confirmOnLoad() {

	loadPendingConfirmations()

	logs.Log.Info("Starting confirmation thread")
	go startUnknownVerificationThread()
	go processPendingConfirmations()
}

func loadPendingConfirmations() {

	logs.Log.Info("Loading pending confirmations")

	pending := 0

	var keysToRemove [][]byte

	err := db.Singleton.View(func(dbTx db.Transaction) error {
		return coding.ForPrefixInt64(dbTx, ns.Prefix(ns.NamespaceEventConfirmationPending), true, func(key []byte, timestamp int64) (bool, error) {

			if dbTx.HasKey(ns.Key(key, ns.NamespaceConfirmed)) {
				// Tx already confirmed => remove the "pending keys"
				keysToRemove = append(keysToRemove, key)
				return true, nil
			}

			pendingConfirmationsQueue <- &PendingConfirmation{key, timestamp}
			pending++

			return true, nil
		})
	})
	if err != nil {
		logs.Log.Panicf("Load pending confirmations failed! Err:", err)
	}

	db.Singleton.Update(func(dbTx db.Transaction) (err error) {
		for _, key := range keysToRemove {
			dbTx.Remove(key) // NamespaceEventConfirmationPending
		}
		return nil
	})

	logs.Log.Infof("Loaded %v pending confirmations", len(pendingConfirmationsQueue))
}

func processPendingConfirmations() {
	pendingConfirmationsWaitGroup.Add(1)
	defer pendingConfirmationsWaitGroup.Done()

	pendingConfirmationsTicker = time.NewTicker(confirmationsTickerInterval)
	for {
		select {
		case <-pendingConfirmationsQueueQuit:
			return

		case <-pendingConfirmationsTicker.C:
			if ended {
				break
			}
			processPendingConfirmationsQueue()
		}
	}
}

func processPendingConfirmationsQueue() {

	seenConfirmations := make(map[string]*struct{})
	for {
		select {

		case pendingConfirmation := <-pendingConfirmationsQueue:
			if ended {
				return
			}

			hash := string(pendingConfirmation.key)

			_, seenAlready := seenConfirmations[hash]
			if seenAlready {
				// We are back at the start of the queue => quit loop
				// Re-add it to the queue (TODO non-blocking write?)
				pendingConfirmationsQueue <- pendingConfirmation
				return
			}
			seenConfirmations[hash] = nil // mark pendingConfirmation as seen

			var confirmed, pending bool

			err := db.Singleton.Update(func(dbTx db.Transaction) (err error) {
				confirmed, pending, err = confirm(pendingConfirmation.key, dbTx)
				if err != nil {
					return err
				}

				if !pending {
					dbTx.Remove(ns.Key(pendingConfirmation.key, ns.NamespaceEventConfirmationPending))
				}
				return nil
			})
			if err != nil {
				if err != db.ErrTransactionConflict {
					logs.Log.Errorf("Error during confirmation: %v", err)
				}
				pending = true
			}

			if pending {
				pendingConfirmationsQueue <- pendingConfirmation
			} else if confirmed {
				totalConfirmations++
			}

		default:
			// Non blocking read is essential here, otherwise it causes a deadlock if the queue is empty
			return
		}
	}
}

func startUnknownVerificationThread() {
	removeOrphanedPendingWaitGroup.Add(1)
	defer removeOrphanedPendingWaitGroup.Done()

	removeOrphanedPendingTicker = time.NewTicker(UNKNOWN_CHECK_INTERVAL)
	for {
		select {
		case <-removeOrphanedPendingTickerQuit:
			return

		case <-removeOrphanedPendingTicker.C:
			if ended {
				break
			}
			db.Singleton.View(func(dbTx db.Transaction) error {
				var toRemove [][]byte
				ns.ForNamespace(dbTx, ns.NamespacePendingConfirmed, false, func(key, _ []byte) (bool, error) {
					if dbTx.HasKey(ns.Key(key, ns.NamespaceHash)) {
						k := make([]byte, len(key))
						copy(k, key)
						toRemove = append(toRemove, k)
					}
					return true, nil
				})

				for _, key := range toRemove {
					logs.Log.Debug("Removing orphaned pending confirmed key", key)
					err := db.Singleton.Update(func(dbTx db.Transaction) error {
						if err := dbTx.Remove(key); err != nil {
							return err
						}
						return confirmChild(ns.Key(key, ns.NamespaceHash), dbTx)
					})
					if err != nil {
						return err
					}
				}
				return nil
			})
		}
	}
}

func confirm(key []byte, dbTx db.Transaction) (newlyConfirmed bool, retryConfirm bool, err error) {

	if dbTx.HasKey(ns.Key(key, ns.NamespaceConfirmed)) {
		// Already confirmed
		return false, false, nil
	}

	data, err := dbTx.GetBytes(ns.Key(key, ns.NamespaceBytes))
	if err != nil {
		// Probably the tx is not yet committed to the database. Simply retry.
		return false, true, nil
	}

	trits := convert.BytesToTrits(data)[:8019]
	tx := transaction.TritsToFastTX(&trits, data)

	if dbTx.HasKey(ns.Key(key, ns.NamespaceEventTrimPending)) && !isMaybeMilestonePart(tx) {
		return false, false, fmt.Errorf("TX behind snapshot horizon, skipping (%v vs %v). Possible DB inconsistency! TX: %v",
			tx.Timestamp,
			snapshot.GetSnapshotTimestamp(dbTx),
			convert.BytesToTrytes(tx.Hash))
	}

	// TODO: This part should be atomic with all the value and confirmes and childs?
	//		 => If there is a DB error, revert DB commit
	err = coding.PutInt64(dbTx, ns.Key(key, ns.NamespaceConfirmed), int64(tx.Timestamp))
	if err != nil {
		return false, true, errors.New("Could not save confirmation status!")
	}

	if tx.Value != 0 {
		_, err := coding.IncrementInt64By(dbTx, ns.AddressKey(tx.Address, ns.NamespaceBalance), tx.Value, false)
		if err != nil {
			return false, true, errors.New("Could not update account balance!")
		}
		if tx.Value < 0 {
			err := coding.PutBool(dbTx, ns.AddressKey(tx.Address, ns.NamespaceSpent), true)
			if err != nil {
				return false, true, errors.New("Could not update account spent status!")
			}
		}
	}

	err = confirmChild(ns.HashKey(tx.TrunkTransaction, ns.NamespaceHash), dbTx)
	if err != nil {
		return false, true, err
	}
	err = confirmChild(ns.HashKey(tx.BranchTransaction, ns.NamespaceHash), dbTx)
	if err != nil {
		return false, true, err
	}
	return true, false, nil
}

func confirmChild(key []byte, dbTx db.Transaction) error {
	if bytes.Equal(key, tipHashKey) {
		// Doesn't need confirmation
		return nil
	}
	if dbTx.HasKey(ns.Key(key, ns.NamespaceConfirmed)) {
		// Already confirmed
		return nil
	}

	if dbTx.HasKey(ns.Key(key, ns.NamespaceEventConfirmationPending)) {
		// Confirmation is already pending
		return nil
	}

	timestamp, err := coding.GetInt64(dbTx, ns.Key(key, ns.NamespaceTimestamp))
	if err == nil {
		err = addPendingConfirmation(key, timestamp, dbTx)
		if err != nil {
			return fmt.Errorf("Could not save child confirm status: %v", err)
		}
	} else if !dbTx.HasKey(ns.Key(key, ns.NamespaceEdge)) && dbTx.HasKey(ns.Key(key, ns.NamespacePendingHash)) {
		err = coding.PutInt64(dbTx, ns.Key(key, ns.NamespacePendingConfirmed), time.Now().Unix())
		if err != nil {
			return fmt.Errorf("Could not save child pending confirm status: %v", err)
		}
	}
	return nil
}

func addPendingConfirmation(key []byte, timestamp int64, dbTx db.Transaction) error {
	err := coding.PutInt64(dbTx, ns.Key(key, ns.NamespaceEventConfirmationPending), timestamp)
	if err == nil {
		// TODO: find a way to add to the queue AFTER the tx has been committed.
		pendingConfirmationsQueue <- &PendingConfirmation{key, timestamp}
	}
	return err
}

/*
func hasConfirmInProgress(key []byte) bool {
	confirmsInProgressLock.RLock()
	defer confirmsInProgressLock.RUnlock()

	_, ok := confirmsInProgress[string(key)]
	return ok
}

func addConfirmInProgress(key []byte) bool {
	if hasConfirmInProgress(key) {
		return false
	}

	confirmsInProgressLock.Lock()
	defer confirmsInProgressLock.Unlock()

	confirmsInProgress[string(key)] = true
	return true
}

func removeConfirmInProgress(key []byte) {
	confirmsInProgressLock.Lock()
	defer confirmsInProgressLock.Unlock()
	k := string(key)
	_, ok := confirmsInProgress[k]
	if ok {
		delete(confirmsInProgress, k)
	}
}
*/

func reapplyConfirmed() {
	logs.Log.Debug("Reapplying confirmed TXs to balances")
	db.Singleton.View(func(dbTx db.Transaction) error {
		x := 0
		return ns.ForNamespace(dbTx, ns.NamespaceConfirmed, false, func(key, _ []byte) (bool, error) {
			txBytes, _ := dbTx.GetBytes(ns.Key(key, ns.NamespaceBytes))
			trits := convert.BytesToTrits(txBytes)[:8019]
			tx := transaction.TritsToFastTX(&trits, txBytes)
			if tx.Value != 0 {
				err := db.Singleton.Update(func(dbTx db.Transaction) error {
					_, err := coding.IncrementInt64By(dbTx, ns.AddressKey(tx.Address, ns.NamespaceBalance), tx.Value, false)
					if err != nil {
						logs.Log.Errorf("Could not update account balance: %v", err)
						return errors.New("Could not update account balance!")
					}
					if tx.Value < 0 {
						err := coding.PutBool(dbTx, ns.AddressKey(tx.Address, ns.NamespaceSpent), true)
						if err != nil {
							logs.Log.Errorf("Could not update account spent status: %v", err)
							return errors.New("Could not update account spent status!")
						}
					}
					return nil
				})
				if err != nil {
					logs.Log.Errorf("Could not apply tx Value: %v", err)
					return false, errors.New("Could not apply Tx value!")
				}
			}
			x = x + 1
			if x%10000 == 0 {
				logs.Log.Debug("Progress", x)
			}
			return true, nil
		})
	})
}
